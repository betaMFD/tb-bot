import { SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('send_to_hidden_channel')
  .setDescription('Send a message to the special channel even if it\'s not accessible')
  .addStringOption(option =>
    option.setName('message')
      .setDescription('The message to send.')
      .setRequired(true));

export async function execute(interaction) {
  // Get the locked room channel
  const lockedRoom = interaction.client.channels.cache.get(process.env.LOCKED_ROOM);

  // Get the message from the interaction
  const message = interaction.options.getString('message');

  // Send the message to the locked room
  await lockedRoom.send(`*${interaction.member.displayName} sent this message:*\n${message}`);

  // Respond to the user that the message was sent successfully
  await interaction.reply({ content: '*Message sent to the locked room.*\n' + message, ephemeral: true });
}
