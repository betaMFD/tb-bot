import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { hideChannel, unhideChannel } from '../utils/channelVisibility.mjs';
const room_id = process.env.LOCKED_ROOM;

export const data = new SlashCommandBuilder()
  .setName('toggle_visibility')
  .setDescription('Manually update special channel visibility')
  .addStringOption(option =>
    option.setName('message')
      .setDescription('Why.')
      .setRequired(true));

export async function execute(interaction) {
  // Get the locked room channel
  const lockedRoom = interaction.client.channels.cache.get(process.env.LOCKED_ROOM);

  // Check if the user can view the locked room
  const member = interaction.guild.members.cache.get(process.env.SUB_USER_ID);
  const overwrite = lockedRoom.permissionOverwrites.cache.get(member.id);
  const canView = !overwrite || (overwrite.allow.has(PermissionFlagsBits.ViewChannel) && !overwrite.deny.has(PermissionFlagsBits.ViewChannel));
  const message = interaction.options.getString('message');

  if (canView) {
    // Hide the channel and send a message
    await hideChannel();
    await interaction.reply({
      content: `<#${room_id}> has been manually hidden from <@${member.user.id}>.\n` +
        `> ${message}`
    });
  } else {
    // Unhide the channel and send a message
    await unhideChannel();
    await interaction.reply({
      content: `<#${room_id}> is **visible again** for <@${member.user.id}>.\n` +
        `> ${message}`
    });
  }
}
