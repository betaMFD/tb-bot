import { PermissionFlagsBits } from 'discord.js';
import {bot} from '../services/bot.mjs';
import { config } from 'dotenv';
config(); // Load environment variables from .env file

const user_id = process.env.SUB_USER_ID;
const room_id = process.env.LOCKED_ROOM;

async function updateChannelVisibility(canView) {
  // Get the guild where you want to set the permissions
  const guild = bot.guild;
  if (!guild) {
    console.log('Guild not found');
    return;
  }

  // Get the channel you want to set permissions for
  const channel = guild.channels.cache.get(room_id);
  if (!channel) {
    console.log('Channel not found');
    return;
  }

  // Get the user you want to set permissions for
  const member = await guild.members.fetch(user_id);
  if (!member) {
    console.log('Member not found');
    return;
  }

  // Set channel permissions for the user
  await channel.permissionOverwrites.create(member, {
    [PermissionFlagsBits.ViewChannel]: canView,
  });

  console.log(
    `Permissions updated: User ${user_id} ${
      canView ? 'can' : "can't"
    } see channel ${room_id}`
  );
}

export const hideChannel = async function () {
  await updateChannelVisibility(false);
};

export const unhideChannel = async function () {
  await updateChannelVisibility(true);
};
