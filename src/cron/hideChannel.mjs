
import { hideChannel } from '../utils/channelVisibility.mjs';


export default {
  // Cron schedule string - runs 8am every week day
  schedule: '1 0 8 * * 1,2,3,4,5',

  // Cron task function
  // hides a channel by setting permissions for the user
  task: async function() {
    try {
      await hideChannel();
    } catch (error) {
      console.error('Error updating permissions:', error);
    }
  }
}
