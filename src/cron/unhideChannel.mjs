import { unhideChannel } from '../utils/channelVisibility.mjs';


export default {
  // Cron schedule string - runs 4:30pm every week day
  schedule: '1 30 16 * * 1,2,3,4,5',

  // Cron task function
  // un-hides a channel by updating permissions for the user
  task: async function() {
    try {
      await unhideChannel();
    } catch (error) {
      console.error('Error updating permissions:', error);
    }
  }
}
