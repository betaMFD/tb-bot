export default {
  // Cron schedule string - runs at 8pm on the 25th of each month
  schedule: '0 20 25 * *',

  // Cron task function
  // sends a message to the general channel
  task: async function(bot) {
    try {
      const channel = bot.generalChannel;
      if (channel) {
        await channel.send("It's the 25th!");
      } else {
        console.error('General channel not found');
      }
    } catch (error) {
      console.error('Error sending message:', error);
    }
  }
};
